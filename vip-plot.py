# -*- coding: utf-8 -*-
"""
Created on Sat Dec 10 16:37:01 2016
@author: wabo
"""

TIME = 0      # - time
NODES = 1     # - number-of-nodes
STABLE = 2    # - molecule: stable
ISGOING = 3   # - molecule: isGoing
CROWDTHR = 4  # - molecule: crowdThr
COUNTA = 5    # - molecule: countA
COUNTB = 6    # - molecule: countB
COUNTC = 7    # - molecule: countC
ARRIVEDA = 8  # - molecule: arrivedA
ARRIVEDB = 9  # - molecule: arrivedB
ARRIVEDC = 10 # - molecule: arrivedC
DISTA = 11    # - molecule: distA
DISTB = 12    # - molecule: distB
DISTC = 13    # - molecule: distC
DISTN = 14    # - molecule: distN
DISTARRIVEDA = 15    # - molecule: distArrivedA
DISTARRIVEDB = 16    # - molecule: distArrivedB
DISTARRIVEDC = 17    # - molecule: distArrivedC
SERVEDA = 18
SERVEDB = 19
SERVEDC = 20
SUMSERVED = 21
AVERAGESERVED = 22
RATIOSERVED = 23
NOTSERVEDA = 24
NOTSERVEDB = 25
NOTSERVEDC = 26
SUMNOTSERVED = 27
AVERAGENOTSERVED = 28
DISTSERVEDA = 29 
DISTSERVEDB = 30 
DISTSERVEDC = 31  
AVGDISTSERVEDA = 32
AVGDISTSERVEDB = 33 
AVGDISTSERVEDC = 34
AVGDISTSERVED  = 35
DISTNOTSERVEDA = 36    
DISTNOTSERVEDB = 37    
DISTNOTSERVEDC = 38
SUMDISTNOTSERVED = 39
SUMDISTWASTED = 40
AVGDISTNOTSERVEDA = 41
AVGDISTNOTSERVEDB = 42
AVGDISTNOTSERVEDC = 43

xmax=27

import numpy as np
import matplotlib.pyplot as plt

params = {'legend.fontsize': 14,
          'legend.loc': 'best',
          'axes.labelsize': 17,
          'axes.titlesize': 20,
          'xtick.labelsize': 17,
          'ytick.labelsize': 17,
          'axes.linewidth': 1.0,
          'font.size': 20,
          'font.family': 'sans-serif',
          'grid.color': 'k',
          'grid.linestyle': ':',
          'grid.linewidth': 0.5,
          'axes.xmargin': 0.0,
          'axes.ymargin': 0.1,
          'axes.axisbelow': False,
          'legend.frameon': False,
          'errorbar.capsize': 3
         }
plt.rcParams.update(params)

outputDirectory = "charts/vip/"
#p = {"outName": "vip", "title": "\"Meeting the celebrity\"", "name": ["_high", "_medium", "_low"], "inputDir":"data/newvip2/"}
p = {"outName": "vip", "title": "\"Meeting the celebrity\"", "name": ["_high"], "inputDir":"data/newvip2/"}
figsize=(7,3)
colors = ['orange', 'green', 'blue', 'red']
mapping = { 
    'flex': { 'short': 'L', 'extended': 'Library' }, 
    'naive': { 'short': 'BB', 'extended': 'BB-Only'  }, 
    '_low': { 'short': 'Low', 'extended': 'Low - '},
    '_medium': { 'short': 'High', 'extended': 'High - '}, 
    '_high': { 'short': 'Asym', 'extended': 'Asymmetric - '},
}
for n in p['name']:
    fig1min = 1000
    print(n)

    inputDirectory = p['inputDir']
    fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    fig3, ax3 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    fig4, ax4 = plt.subplots(nrows=1, ncols=1, figsize=figsize)

    for n2 in ['flex', 'naive']:
        avg = np.genfromtxt(inputDirectory + n2 + n + "-mean_freq-0.5.txt", dtype=float, delimiter=' ')
        std = np.genfromtxt(inputDirectory + n2 + n + "-err_freq-0.5.txt", dtype=float, delimiter=' ') 
        X               = np.array([x[TIME] for x in avg])/60 # time
        devices         = np.array([x[NODES] for x in avg]) # device number
        stable          = np.array([x[NODES] - x[STABLE] for x in avg]) # number of variations
        stablestd       = np.array([x[STABLE] for x in std])
        crowdthr        = np.array([x[CROWDTHR] for x in avg]) 
        arriveda        = np.array([x[ARRIVEDA] for x in avg]) 
        arrivedastd     = np.array([x[ARRIVEDA] for x in std])
        arrivedb        = np.array([x[ARRIVEDB] for x in avg]) 
        arrivedbstd     = np.array([x[ARRIVEDB] for x in std])    
        arrivedc        = np.array([x[ARRIVEDC] for x in avg]) 
        arrivedcstd     = np.array([x[ARRIVEDC] for x in std])
        avgdistserveda      = np.array([x[AVGDISTSERVEDA] for x in avg])
        avgdistservedastd   = np.array([x[AVGDISTSERVEDA] for x in std])
        avgdistservedb      = np.array([x[AVGDISTSERVEDB] for x in avg])
        avgdistservedbstd   = np.array([x[AVGDISTSERVEDB] for x in std])
        avgdistservedc      = np.array([x[AVGDISTSERVEDC] for x in avg])
        avgdistservedcstd   = np.array([x[AVGDISTSERVEDC] for x in std])
        distnotserveda      = np.array([x[DISTNOTSERVEDA] for x in avg])
        distnotservedastd   = np.array([x[DISTNOTSERVEDA] for x in std])
        distnotservedb      = np.array([x[DISTNOTSERVEDB] for x in avg])
        distnotservedbstd   = np.array([x[DISTNOTSERVEDB] for x in std])
        distnotservedc      = np.array([x[DISTNOTSERVEDC] for x in avg])
        distnotservedcstd   = np.array([x[DISTNOTSERVEDC] for x in std])
        distn               = np.array([x[DISTN] for x in avg]) 
        distnstd            = np.array([x[DISTN] for x in std])

        style = '-'
        marker = 's'
        c = 'blue'
        if n2.startswith('naive'):
            style = '--'
            marker = 'o'
            c = 'green'
        #if n2.startswith('flex'):   
        #    ax1.plot(X, devices + 10, color=colors[0], label="Devices", linewidth=3)
        ax1.plot(X, stable + 10, color=c, label=mapping[n2]['short'], linestyle = style, linewidth=3)
        ax1.plot(X, stable + 10 - stablestd, color=c, linestyle = style, linewidth=0.5)
        ax1.plot(X, stable + 10 + stablestd, color=c, linestyle = style, linewidth=0.5)
        
        if n.endswith("low"):
            fig1min = min(np.min(stable[100:]), fig1min)
        else:
            fig1min = min(np.min(stable[20:]), fig1min)
            
        if n2.startswith('flex'):
            ax2.plot(X, crowdthr, color=colors[3], label="Thr")
        ax2.plot(X, arriveda, color=colors[0], linestyle = style, label = mapping[n2]['short'] + "-A", linewidth=3)
        ax2.plot(X, arriveda + arrivedastd, color=colors[0], linestyle = style, linewidth=0.5)
        ax2.plot(X, arriveda - arrivedastd, color=colors[0], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedb, color=colors[1], linestyle = style, label = mapping[n2]['short'] + "-B", linewidth=3)
        ax2.plot(X, arrivedb + arrivedbstd, color=colors[1], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedb - arrivedbstd, color=colors[1], linestyle = style, linewidth=0.5) 
        ax2.plot(X, arrivedc, color=colors[2], linestyle = style, label = mapping[n2]['short'] + "-C", linewidth=3)
        ax2.plot(X, arrivedc + arrivedcstd, color=colors[2], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedc - arrivedcstd, color=colors[2], linestyle = style, linewidth=0.5)
        #ax2.set_ylim(ymin=0, ymax = max(arrivedb[-1], crowdthr[0]) + 100)
           
        ax3.plot(X, avgdistserveda, color=colors[0], label = mapping[n2]['short'] + " A", linestyle = style)
        ax3.plot(X, avgdistservedb, color=colors[1], label = mapping[n2]['short'] + " B", linestyle = style) 
        ax3.plot(X, avgdistservedc, color=colors[2], label = mapping[n2]['short'] + " C", linestyle = style)        

        ax4.plot(X, distnotserveda, color=colors[0], label = mapping[n2]['short'] + " A", linestyle = style)
        ax4.plot(X, distnotservedb, color=colors[1], label = mapping[n2]['short'] + " B", linestyle = style) 
        ax4.plot(X, distnotservedc, color=colors[2], label = mapping[n2]['short'] + " C", linestyle = style)  
        ax4.plot(X, distnotservedc, color=colors[3], label = mapping[n2]['short'] + " not served", linestyle = style)  
    
    ax1.set_xlim(xmin=0, xmax=xmax)
    #ax1.set_yscale('log')
    ax1.set_ylim(ymin=fig1min, ymax=1002)
    ax1.set_title(mapping[n]['extended'] + ' stabilization')
    ax1.set_ylabel("Devices")
    ax1.set_xlabel("Simulation time (mins)")
    #ax1.legend(ncol=4)
    ax2.set_title(mapping[n]['extended'] + ' attendees to POI')
    ax2.set_ylabel("Devices")
    ax2.set_xlabel("Simulation time (mins)")
    ax2.set_ylim(ymin=0)
    ax2.set_xlim(xmin=0, xmax=xmax)
    #ax2.legend(ncol=4)
    box = ax2.get_position()
    ax2.set_position([box.x0, box.y0, box.width * 0.7, box.height])
    ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)
    
    ax3.set_ylim(ymin=0)
    ax3.set_xlim(xmin=0, xmax=xmax)
    ax3.set_title(mapping[n]['extended'] + ' average distance to POI - served')
    ax3.set_ylabel("Distance (m)")
    ax3.set_xlabel("Simulation time (mins)")
    #ax3.legend()
    ax4.set_ylim(ymin=0)
    ax4.set_xlim(xmin=0, xmax=xmax)
    ax4.set_title(mapping[n]['extended'] + ' sum of distances to POI - not served')
    ax4.set_ylabel("Distance (m)")
    ax4.set_xlabel("Simulation time (mins)")
    #ax4.legend()
    
    fig1.tight_layout()
    fig1.show() 
    fig2.tight_layout()
    fig2.show() 
    fig3.tight_layout()
    fig3.show() 
    fig4.tight_layout()
    fig4.show() 
    
    savename = ''
    if n.endswith('high'):
        savename = '_asymm'
    elif n.endswith('medium'):
        savename = '_high'
    else:
        savename = '_low'
        
    handles,labels = ax1.get_legend_handles_labels()
    fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
    axe.axis('off')
    axe.legend(handles, labels)
    fig.savefig(outputDirectory + p['outName'] + '_legend_stable.pdf',bbox_inches='tight')
    handles,labels = ax2.get_legend_handles_labels()
    fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
    axe.axis('off')
    axe.legend(handles, labels)
    fig.savefig(outputDirectory + p['outName'] + '_legend_allocated.pdf',bbox_inches='tight')
        
    fig1.savefig(outputDirectory + p['outName'] + savename +'_1stable.pdf',bbox_inches='tight')
    fig2.savefig(outputDirectory + p['outName'] + savename +'_2crowd.pdf',bbox_inches='tight')
#    fig3.savefig(outputDirectory + p['outName'] + savename +'_3.pdf')
#    fig4.savefig(outputDirectory + p['outName'] + savename +'_4.pdf')