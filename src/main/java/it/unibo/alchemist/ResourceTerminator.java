package it.unibo.alchemist;

import it.unibo.alchemist.boundary.interfaces.OutputMonitor;
import it.unibo.alchemist.model.implementations.nodes.ProtelisNode;
import it.unibo.alchemist.model.implementations.times.DoubleTime;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Reaction;
import it.unibo.alchemist.model.interfaces.Time;

public class ResourceTerminator<T> implements OutputMonitor<T>{
    private static final long serialVersionUID = 1L;
    private Time time;
    public ResourceTerminator() { time = new DoubleTime(0); }

    @Override
    public void finished(Environment<T> env, Time time, long step) {
        System.out.println("Done.");
    }

    @Override
    public void initialized(Environment<T> env) {
    }

    @Override
    public void stepDone(Environment<T> env, Reaction<T> r, Time time, long step) {
//        if (time.toDouble() > 600 && time.toDouble() - this.time.toDouble() > 10) {
//            double arrivedA = 0;
//            double arrivedB = 0;
//            double arrivedC = 0;
//            double arrivedD = 0;
//            double neededA = 0;
//            double neededB = 0;
//            double neededC = 0;
//            double neededD = 0;
//            double allResources = 0;
//            boolean flag = true;
//            for (Node<?> n: env.getNodes()) {
//                try {
//                    ProtelisNode pn = (ProtelisNode) n;
//                    allResources += (double) pn.get("resources");
//                    arrivedA += (double) pn.get("countA");
//                    arrivedB += (double) pn.get("countB");
//                    arrivedC += (double) pn.get("countC");
//                    arrivedD += (double) pn.get("countD");
//                    neededA += (double) pn.get("neededA");
//                    neededB += (double) pn.get("neededB");
//                    neededC += (double) pn.get("neededC");
//                    neededD += (double) pn.get("neededD");
//                } catch (Exception ex) {
//                    flag = false;
//                }
//            }
//            double arrivedResources = arrivedA + arrivedB + arrivedC + arrivedD;
//            //System.out.println(time.toDouble() + " | " + allResources + " | " + arrivedA + " " + arrivedB + " " +arrivedC + " " +arrivedD + " | " +neededA + " " +neededB + " " +neededC + " " +neededD);
//            if (flag && allResources > 0 && (arrivedResources == allResources || (arrivedA >= neededA && arrivedB >= neededB && arrivedC >= neededC && arrivedD >= neededD))) { 
//                env.getSimulation().terminate(); 
//                System.out.println(time.toDouble() + ": Resource Done.");
//            }
//            this.time = time;
//        }
    }
}